package vulnerableDB

import (
	"database/sql"
	"fmt"
	"main/helpers"

	_ "github.com/lib/pq"
)

type User struct {
	ID int	`json:"id" db:"id"`
	Username string `json:"username" db:"username"`
	Email string	`json:"email" db:"email"`
	Accounts []Account `json:"account"`
}

type Account struct {
	ID int `json:"id" db:"id"`
	Name string `json:"name" db:"name"`
	Balance int `json:"balance" db:"balance"`
	UserID  int `json:"user_id" db:"user_id"`
}

func ConnectDB() *sql.DB {
	db, err := sql.Open("postgres", "host=127.0.0.1 port=5432 user=postgres dbname=postgres password=root sslmode=disable")
	helpers.HandleErr(err)
	return db
}

func DBCall(query string) *sql.Rows {
	db := ConnectDB()

	call, err := db.Query(query)

	helpers.HandleErr(err)
	return call
}


func VulnerableLogin(username string, pass string) []User {
	// cara ini termasuk untuk membandingkan password dengan meng hash nya
	password := helpers.HashOnlyVulnerable([]byte(pass))
	results := DBCall("SELECT id, username, email FROM users x WHERE username='" + username + "' AND password='" + password + "'")
	var users []User

	for results.Next() {
		var user User
		err := results.Scan(&user.ID, &user.Username, &user.Email)
		helpers.HandleErr(err)
		accounts := DBCall("SELECT id, name, balance FROM accounts x WHERE user_id=" + fmt.Sprint(user.ID) + "")
		var userAccounts []Account

		for accounts.Next() {
			var account Account
			err := accounts.Scan(&account.ID, &account.Name, &account.Balance)
			helpers.HandleErr(err)
			userAccounts = append(userAccounts, account)
		}

		user.Accounts = userAccounts
		users = append(users, user)
	}
	defer results.Close()
	return users
}