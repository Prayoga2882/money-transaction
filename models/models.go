package models

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"main/helpers"
	"main/migration"
	"main/vulnerableDB"
	"net/http"

	"github.com/gorilla/mux"
)

type Login struct {
	Username string
	Password string
}

type Response struct {
	Data []vulnerableDB.User
}

type ErrResponse struct {
	Message string
}

type Transfer struct {
	ID                   string `json:"id"`
	AccountOriginId      int    `json:"account_origin_id"`
	AccountDestinationId int    `json:"account_destination_id"`
	Amount               int    `json:"amount"`
	CreatedAt            string `json:"created_at"`
}

type allTranfers []Transfer

var transfers = allTranfers{}

func login(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	helpers.HandleErr(err)

	var formattedBody Login
	err = json.Unmarshal(body, &formattedBody)
	helpers.HandleErr(err)
	login := vulnerableDB.VulnerableLogin(formattedBody.Username, formattedBody.Password)

	if len(login) > 0 {
		resp := Response{Data: login}
		json.NewEncoder(w).Encode(resp)
	} else {
		resp := ErrResponse{Message: "Wrong username or password"}
		json.NewEncoder(w).Encode(resp)
	}
}

func users(w http.ResponseWriter, r *http.Request) {
	db := vulnerableDB.ConnectDB()
	rows, err := db.Query("SELECT id, username, email FROM users")
	if err != nil {
		log.Fatal(err)
	}
	var people []vulnerableDB.User
	for rows.Next() {
		var person vulnerableDB.User
		rows.Scan(&person.ID, &person.Username, &person.Email)

		rows, err := db.Query("SELECT id, name, balance FROM accounts")
		if err != nil {
			log.Fatal(err)
		}
		var account []vulnerableDB.Account
		for rows.Next() {
			var count vulnerableDB.Account
			rows.Scan(&count.ID, &count.Name, &count.Balance)

			account = append(account, count)
		}
		person.Accounts = account
		people = append(people, person)
	}

	peopleBytes, _ := json.MarshalIndent(people, "", "\t")

	w.Header().Set("Content-Type", "application/json")
	w.Write(peopleBytes)

	defer rows.Close()
	defer db.Close()
}

func accounts(w http.ResponseWriter, r *http.Request) {
	db := vulnerableDB.ConnectDB()
	rows, err := db.Query("SELECT id, name, balance FROM accounts")
	if err != nil {
		log.Fatal(err)
	}
	var people []vulnerableDB.Account
	for rows.Next() {
		var person vulnerableDB.Account
		rows.Scan(&person.ID, &person.Name, &person.Balance)
		people = append(people, person)
	}
	peopleBytes, _ := json.MarshalIndent(people, "", "\t")
	w.Header().Set("Content-Type", "application/json")
	w.Write(peopleBytes)
}

func CreateAccount(w http.ResponseWriter, r *http.Request) {
	db := vulnerableDB.ConnectDB()

	var p migration.User
	err := json.NewDecoder(r.Body).Decode(&p)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	sqlStatement := `INSERT INTO users (username, email, password) VALUES ($1, $2, $3)`
	_, err = db.Exec(sqlStatement, p.Username, p.Email, p.Password)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		panic(err)
	}

	w.WriteHeader(http.StatusOK)
	defer db.Close()
}

func transaction(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	var transfer Transfer
	reqBody, err := ioutil.ReadAll(r.Body)
	if err != nil {panic(err)}
	if err := r.Body.Close(); err != nil {panic(err)}

	if err := json.Unmarshal(reqBody, &transfer); err != nil {
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		w.WriteHeader(422)
		if err := json.NewEncoder(w).Encode(err); err != nil {
			panic(err)
		}
	}
	db := vulnerableDB.ConnectDB()
	rows, err := db.Query("SELECT id, name, balance, user_id FROM accounts")
	if err != nil {
		log.Fatal(err)
	}
	var accounts []vulnerableDB.Account
	for rows.Next() {
		var person vulnerableDB.Account
		rows.Scan(&person.ID, &person.Name, &person.Balance, &person.UserID)
		accounts = append(accounts, person)
	}
	// for _, i := range accounts
	for i := 0; i < len(accounts); i++ {
		if accounts[i].ID == transfer.AccountOriginId {
			if accounts[i].Balance-transfer.Amount < 0 {
				w.Header().Set("Content-Type", "application/json; charset=UTF-8")
				w.WriteHeader(400)
				w.Write([]byte("AccountOriginId insufficient funds!"))
				return
			}
		}
	}

	var updateBalance int
	var updateAmount int

	for i := 0; i < len(accounts); i++ {
		if accounts[i].UserID == transfer.AccountOriginId {
			updateBalance = accounts[i].Balance - transfer.Amount
		}
		if accounts[i].UserID == transfer.AccountDestinationId {
			updateAmount = accounts[i].Balance + transfer.Amount
		}
	}

	transfers = append(transfers, transfer)

	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(transfer)

	err = QueryUpdateBalace(db, updateBalance, transfer.AccountOriginId)
	if err != nil {
		panic(err)
	}

	err = QueryUpdateBalace(db, updateAmount, transfer.AccountDestinationId)
	if err != nil {
		panic(err)
	}

	defer db.Close()
}

func QueryUpdateBalace(db *sql.DB, balance, userID int) error{
	// db := vulnerableDB.ConnectDB()
	sqlStatement1 := `UPDATE accounts SET balance = $1 WHERE user_id = $2;`
	_, err := db.Exec(sqlStatement1, balance, userID) 
	if err != nil {
		return err
	}
	return nil
}

func Routes() {
	router := mux.NewRouter()

	router.HandleFunc("/create", CreateAccount).Methods("POST")
	router.HandleFunc("/login", login).Methods("POST")
	router.HandleFunc("/users", users).Methods("GET")
	router.HandleFunc("/accounts", accounts).Methods("GET")
	router.HandleFunc("/transfer", transaction).Methods("POST")


	fmt.Println("App is working on port :8080")
	log.Fatal(http.ListenAndServe(":8080", router))

}
