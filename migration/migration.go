package migration

import (
	"main/helpers"
	"main/interfaces"

	"github.com/jinzhu/gorm"
	"golang.org/x/crypto/bcrypt"
)

type User struct {
	gorm.Model
	Username string
	Email    string
	Password string
}

type Account struct {
	gorm.Model
	Type    string
	Name    string
	Balance float64
	UserID  uint
}


func ConnectDB() *gorm.DB {
	db, err := gorm.Open("postgres", "host=127.0.0.1 port=5432 user=postgres dbname=postgres password=root sslmode=disable")
	helpers.HandleErr(err)
	return db
}

func HashAndSalt(pass []byte) string {
	hashed, err := bcrypt.GenerateFromPassword(pass, bcrypt.MinCost)
	helpers.HandleErr(err)

	return string(hashed)
}

// func createAccounts() {
// 	db := ConnectDB()

// 	users := [2]User{
// 		{Username: "Prayoga", Email: "prayoga@gmail.com"},
// 		{Username: "Boedihartoyo", Email: "boedihartoyo@gmail.com"},
// 	}

// 	for i := 0; i < len(users); i++ {
// 		// Correct one way
// 		// generatedPassword := helpers.HashAndSalt([]byte(users[i].Username))
// 		generatedPassword := helpers.HashOnlyVulnerable([]byte(users[i].Username))
// 		user := User{Username: users[i].Username, Email: users[i].Email, Password: generatedPassword}
// 		db.Create(&user)

// 		account := Account{Type: "Daily Account", Name: string(users[i].Username + "'s" + " account"), Balance: uint(10000 * int(i+1)), UserID: user.ID}
// 		db.Create(&account)
// 	}
// 	defer db.Close()
// }

func Migrate() {
	db := ConnectDB()
	db.AutoMigrate(&User{}, &Account{})
	defer db.Close()

	// alasan saya uncomment supaya tidak terjadi pembuatan data terus menerus saat program di jalankan
	// createAccounts()
}

func MigrateTransactions() {
	Transactions := &interfaces.Transaction{}
	db := ConnectDB()
	db.AutoMigrate(&Transactions)
	defer db.Close()
}