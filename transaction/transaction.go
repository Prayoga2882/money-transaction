package transaction

import (
	"main/interfaces"
	"main/migration"
)

func CreateTransaction(From uint, To uint, Amount int) {
	db := migration.ConnectDB()
	transaction := &interfaces.Transaction{From: From, To: To, Amount: Amount}
	db.Create(&transaction)

	defer db.Close()
}